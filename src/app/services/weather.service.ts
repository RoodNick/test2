import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import {WeatherInfo} from '../models/weather-info';

const url: string = 'http://api.openweathermap.org/data/2.5/find?';
const key: string = '&APPID=08b3b66edf973f7c49319df0aba3af90';

@Injectable()
export class WeatherService {

  constructor(private http: Http) {
  }

  getWeather(city: string): Observable<WeatherInfo> {
    return this.http
      .get(url + `q=${city}&type=accurate&units=metric` + key)
      .map(response => {
        let json = response.json();

        console.log("response: " + response);
        console.log("json: " + json);
        console.log("text: " + response.text());
        return this.parseJson(city, json.list[0]);
      }).catch(this.handleError)
  }

  private parseJson(city: string, json: any) {
    if (json.name == city)
    return new WeatherInfo(json.main.temp, json.main.pressure, json.wind.speed, json.wind.deg);

    return null;
  }

  private handleError(error: Response) {
    console.error("Ошибка: " + error);
    return Observable.of<WeatherInfo>(null);
  }
}
