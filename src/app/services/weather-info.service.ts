import {Injectable} from "@angular/core";
import {WeatherInfo} from "../models/weather-info";
import {Observable, Subject} from "rxjs/Rx";
import {WeatherService} from "./index";

@Injectable()
export class WeatherInfoService {
  private searchWeatherInfo = new Subject<string>();

  constructor(private weatherService: WeatherService) {
  }

  weatherInfo$: Observable<WeatherInfo> =
    this.searchWeatherInfo
      .debounceTime(500)
      .distinctUntilChanged()
      .switchMap(city => this.weatherService.getWeather(city));


  search(city: string): void {
    this.searchWeatherInfo.next(city);
  }
}
