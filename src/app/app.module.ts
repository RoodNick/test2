import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppRoutingModule} from './app-routing.module';

/*// Imports for loading & configuring the in-memory web api
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './in-memory-data.service';*/

import {AppComponent} from './app.component';
import {WeatherComponent} from "./components/weather";
import {WeatherService} from "./services/weather.service";
import {WeatherInfoService} from "./services/weather-info.service";
import {PressureConverterPipe} from "./pipes/pressure-converter.pipe";
import {DegreesConverterPipe} from "./pipes/degrees-converter";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    WeatherComponent,
    PressureConverterPipe,
    DegreesConverterPipe
  ],
  providers: [WeatherService, WeatherInfoService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
