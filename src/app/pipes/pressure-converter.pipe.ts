import { Pipe, PipeTransform } from '@angular/core';

const factor: number = 0.750062;

@Pipe({name: 'pressConv'})
export class PressureConverterPipe implements PipeTransform {
  transform(value: number): number {
    return value * factor;
  }
}


