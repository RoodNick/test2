import { Pipe, PipeTransform } from '@angular/core';

const factor: number = 0.750062;

@Pipe({name: 'degrConv'})
export class DegreesConverterPipe implements PipeTransform {
  transform(value: number): string {
    let val = (value + 22.5) / 45;
    if (val => 8 || val <= 1) {
      return "С";
    }
    if (val < 2) {
      return "С/В"
    }
    if (val <= 3) {
      return "В";
    }
    if (val < 4) {
      return "Ю/В"
    }
    if (val <= 5) {
      return "Ю";
    }
    if (val < 6) {
      return "Ю/З"
    }
    if (val <= 7) {
      return "З";
    }
    return "С/З"
  }
}


