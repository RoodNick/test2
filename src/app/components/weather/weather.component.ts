import {Component, OnInit} from '@angular/core';
import {WeatherInfoService} from "../../services/weather-info.service";
import {WeatherInfo} from "../../models/weather-info";

@Component({
  selector: 'weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.styl']
})
export class WeatherComponent implements OnInit {
  city: string = "";
  weatherInfo: WeatherInfo;
  notFound: boolean = false;

  constructor(private weatherInfoService: WeatherInfoService) {
  }

  ngOnInit(): void {
    this.weatherInfoService.weatherInfo$.subscribe(
      info => {
        console.log("info " + info);
        if (info) {
          this.notFound = false;
          this.weatherInfo = info;
        } else {
          this.notFound = true;
        }
      }
    )
  }

  search(city: string) {
    if (city && city.trim().length > 0) {
      this.weatherInfoService.search(city.trim());
    }
  }
}
