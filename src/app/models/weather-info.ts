export class WeatherInfo {
  private _temp: number;
  private _pressure: number;
  private _wind_speed: number;
  private _wind_deg: number;

  get wind_deg(): number {
    return this._wind_deg;
  }

  get wind_speed(): number {
    return this._wind_speed;
  }

  get pressure(): number {
    return this._pressure;
  }

  get temp(): number {
    return this._temp;
  }

  constructor(temp: number, pressure: number, wind_speed: number, wind_deg: number) {
    this._temp = temp;
    this._pressure = pressure;
    this._wind_speed = wind_speed;
    this._wind_deg = wind_deg;
  }
}
